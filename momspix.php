<?php

/**
 * Plugin Name:     MomsPix
 * Plugin URI:      https://gitlab.com/calevans/momspix_api
 * Description:     Defines the API endpoints necessary for pictures to be sent to frames controlled by momspix
 * Author:          Cal Evans
 * Author URI:      https://blog.calevans.com
 * Text Domain:     momspix_api
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         wp_podcast_api
 */

/**
 * Here's what I am thinking
 * Endpoint /momspix/pictures
 *   POST - Send a picture to be processed for a given frame. Message reciver is the name of the frame.
 *
 */
require_once dirname(__FILE__) . '/vendor/autoload.php';
require_once dirname(__FILE__) . '/data/Evans.php'; // Personal holidays

use Eicc\HolidayCalculator\HolidayCalculator;
use Eicc\HolidayCalculator\HolidayCollections\US;
use Eicc\HolidayCalculator\HolidayCollections\USFederal;

add_action('rest_api_init', 'momspix_api_init_route');

/**
 * Kick things off
 */
function momspix_api_init_route()
{
  $hc = new HolidayCalculator(new USFederal());
  $hc->addHolidays(new US());
  $hc->addHolidays(new Evans());
  ( new MomsPix\MomsPix($hc) )->initialization();
}
