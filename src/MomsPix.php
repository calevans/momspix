<?php

namespace MomsPix;

use Eicc\HolidayCalculator\HolidayCalculator;

class MomsPix
{
  const VENDOR  = 'eicc';
  const PACKAGE = 'momspix';
  const ENTITY  = 'pictures';
  const VERSION = 1;
  private const CURRENT_IMAGE_PERCENTAGE = .6;
  private const HOLIDAY_IMAGE_PERCENTAGE = .1;
  private const HOLIDAY_CAT_ID = 7;

  /**
   * The namespace
   *
   * @var string $namespace
   */
  protected $namespace     = '';

  /**
   * The type of post we are manipulating
   *
   * @var string $postType
   */
  protected $postType = 'post';

  /**
   * Constructor
   */
  public function __construct(protected HolidayCalculator $holidayCalculator)
  {
    $this->namespace = self::VENDOR .
      '/' . self::PACKAGE .
      '/v' . self::VERSION;

    $this->uri = '/' . self::ENTITY;

  }

    public function initialization()
  {
    $this->registerRoutes();
  }

  public function registerRoutes() : void
  {
    register_rest_route(
      $this->namespace,
      $this->uri,
      [
        [
          'methods'             => \WP_REST_Server::READABLE ,
          'callback'            => [$this, 'getPictures'],
          'permission_callback' => [$this, 'devicePermissionCheck'],
        ],
        ]
    );

  }

  public function devicePermissionCheck($request) : bool
  {
    $userId = wp_validate_application_password(false);

    if (!$userId) {
      return false;
    }

    return true;
  }

  public function getPictures($request)
  {
    $totalCurrent = 0;
    $totalHoliday = 0;

    $deviceName =  $request->get_param('deviceName') ?? '';
    $count =  $request->get_param('count') ?? 100;

    $userId = wp_validate_application_password(false);
    $user=get_user_by('ID', $userId);
    $date = new \DateTimeImmutable();

    $slugs = $this->holidayCalculator->getSlugs($date);

    $media_query = new \WP_Query(
    [
      'post_type' => 'attachment',
      'post_status' => 'inherit',
      'posts_per_page' => -1,
      'cat'=> array('-' .self::HOLIDAY_CAT_ID),
      'category_name' => $deviceName
    ]
  );

    $list = [
      'current' => [],
      'archive' => [],
      'holiday' => []
    ];

    $cutoff = new \DateTimeImmutable();
    $cutoff = $cutoff->sub(new \DateInterval('P1M'));

    foreach ($media_query->posts as $post) {
        $postDate = new \DateTimeImmutable($post->post_date);
        if ($postDate < $cutoff) {
          $list['archive'][] =
            [
              'post' => $post,
              'url' => $post->guid
            ];
            continue;
          }
        $list['current'][] =
          [
            'post' => $post,
            'url' => $post->guid
          ];
    }
    $list['holiday'] = $this->getHolidayImages($slugs, $deviceName);
    $holiday = [];

    $payload = [];
    $current = $list['current'];
    shuffle($current);
    $totalCurrent = $count * self::CURRENT_IMAGE_PERCENTAGE;
    $current = array_slice($current,0,$totalCurrent);

    if (count($current)-1<$totalCurrent) {
      $totalCurrent = count($current)-1;
    }
    if (count($list['holiday'])>0) {
      $holiday = $list['holiday'];
      shuffle($holiday);
      $totalHoliday = $count * self::HOLIDAY_IMAGE_PERCENTAGE;
      $holiday = array_slice($holiday,0,$totalHoliday);
      if (count($holiday)-1<$totalHoliday) {
        $totalHoliday = count($holiday)-1;
      }
    }

    $remaining = $count-$totalCurrent-$totalHoliday;
    $archive = $list['archive'];
    shuffle($archive);
    $archive = array_slice($archive,0,$remaining);
    $payload = [];
    foreach (array_merge($current,$holiday,$archive) as $thisImage) {
      $payload[] = $thisImage['url'];
    }
    return rest_ensure_response( $payload );

  }


    /**
     * Validate that the sender has an account and that they have access to
     * this device.
     */
    public function validateSender(
        string $emailAddress,
        string $deviceName
    ): bool {

        $thisUser = get_user_by('email', $emailAddress);

        if (! $thisUser) {
          return false;
        }

        // need something here. Make sure the person uploading an image has access to that device.

        return true;
    }

    private function getHolidayImages($slugs,$deviceName) : array
    {
      $returnValue = [];
      foreach($slugs as $slug) {


        $media_query = new \WP_Query(
          [
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'posts_per_page' => -1,
            'category_name' => $slug . '+' . $deviceName
          ]
        );

        foreach ($media_query->posts as $post) {
          $returnValue[] =
            [
              'post'=>$post,
              'url' => $post->guid
            ];
        }
      }
      return $returnValue;
    }

}
